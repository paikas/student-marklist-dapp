const SubjectList = artifacts.require('SubjectList')

contract('SubjectList', () => {
    it('should deploy smart contract properly', async () =>{
        const subjectList = await SubjectList.deployed();
        // console.log(SubjectList.address);
        assert(subjectList.address !=='')
    })
    it('test adding a subject', async () => {//it declares a test funcion to check if adding subject to the 'subjectList' contract works as expected
        const subjectList = await SubjectList.deployed();
        const subjectCid = 1;
        await subjectList.createSubject(subjectCid, "Analytical and Critical Thinking");
        const subjectCount = await subjectList.subjectsCount();
        const subject = await subjectList.subjects(subjectCid);
        assert.equal(subjectCount, 1);
        assert.equal(subjectCid, 1);
      });
    
      it('test finding subjects', async () => {
        const s = await SubjectList.deployed();
        await s.createSubject(2, "Maths");
        await s.createSubject(3, "Science");
        await s.createSubject(4, "Python");
        await s.createSubject(5, "Java");
        await s.createSubject(6, "Solidity");
        await s.createSubject(7, "Programming for blockchain");
        
        const count = await s.subjectsCount();
        assert.equal(count, 7);
      
        const subject = await s.findSubject(4);
        assert.equal(subject.name, "Python");
        assert.notEqual(subject.name, "");
    });

    it("should mark retired", async() => {
        const s = await SubjectList.deployed();
        s.markRetired(1)
        const t = await s.findSubject(1);
        assert.equal(false,t.retired)
    })

    it("should update subject", async () => {
        const s = await SubjectList.deployed();
        await s.updateSubject(2, "maths", "math102");
        await s.updateSubject(3, "dzo", "dzo102");

        const subject = await s.findSubject(3);
        assert.equal(subject.name,"dzo102");
        assert.equal(subject.code,"dzo");


    })
})


