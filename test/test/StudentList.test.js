//import contract
const StudentList = artifacts.require('StudentList')

//use contract to write test
//variable:account => all acc in blockchain
contract('StudentList', (account) => {
    //making sure contract is deployed
    beforeEach(async () => {
        this.studentList = await StudentList.deployed()
    })

    //testing deployed student contract
    it('deploys successfully', async ()=> {
        //geting address
        const address = await this.studentList.address
        //test for valid address
        isValidAddress(address)
    })

    it('testing adding students', async() => {
        return StudentList.deployed().then((instance)=>{
            s = instance;
            studentCid = 1;
            return s.createStudent(studentCid, 'Ugyen Lhuendup');

        }).then((transaction) => {
            isValidAddress(transaction.tx)
            isValidAddress(transaction.receipt.blockHash);
            return s.studentsCount()
        }).then((count) => {
            assert.equal(count, 1)
            return s.students(1);
        }).then((student) => {
            assert.equal(student.cid, studentCid)
        })
    })

    it ('test finding students', async () => {
        return StudentList.deployed().then(async (instance) => {
            s = instance;
            studentCid = 2;
            return s.createStudent(studentCid++, "Tshering Yangzom").then(async (tx) =>{
                return s.createStudent(studentCid++, "Sonam Dorji").then(async (tx) =>{
                    return s.createStudent(studentCid++, "Pema Yangzom").then(async (tx) =>{
                        return s.createStudent(studentCid++, "Dorji Sonam").then(async (tx) =>{
                            return s.createStudent(studentCid++, "Gawa Drukpa").then(async (tx) =>{
                                return s.createStudent(studentCid++, "Pela chemo").then(async (tx) =>{
                                    return s.studentsCount().then(async (count) => {
                                        assert.equal(count, 7)

                                        return s.findStudent(5).then(async(student) => {
                                            assert.equal(student.name, "Dorji Sonam")
                                        })
                                    })
                                })
                            })
                        })
                    })
                }) 
            })
        })
    })

    it('test mark graduate students', async ()=> {
        return StudentList.deployed().then(async (instance) => {
            s= instance;
            return s.findStudent(1).then(async (ostudent) => {
                assert.equal(ostudent.name,"Ugyen Lhuendup")
                assert.equal(ostudent.graduated, false)
                return s.markGraduated(1).then(async (transaction) => {
                    return s.findStudent(1).then(async (nstudents) => {
                        assert.equal(nstudents.name, "Ugyen Lhuendup")
                        assert.equal(nstudents.graduated, true)
                        return
                    })
                })
            })
        })
        
    })

})

//check if the address is valid
function isValidAddress(address){
    assert.notEqual(address,0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}