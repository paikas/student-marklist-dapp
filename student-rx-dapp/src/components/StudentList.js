import React, { Component } from "react";

class StudentList extends Component {
  render() {
    return (
      <form className="p-5 border" onSubmit={(event) => {
        event.preventDefault()
        this.props.createStudent(this.cid.value, this.student.value)
      }}>
        <h2>Add Student</h2>
        <label>CID</label>
        <input
          id="newCID"
          type="text"
          className="form-control"
          ref={(input) => {this.cid = input;}}
          placeholder="CID"
          required
        />
        <label>Student Name</label>
        <input
          id="newStudent"
          type="text"
          className="form-control"
          ref={(input) => {this.student = input;}}
          placeholder="Student Name"
          required
        />
        <button className="button btn-primary" type="submit">
          Add
        </button>
      </form>
    );
  }
}

export default StudentList;