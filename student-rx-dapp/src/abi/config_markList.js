export const MARK_LIST_ADDRESS = '0xF84B214e470dfAeDE2B2F1Ed2017258db013113D'

export const MARK_LIST_ABI =  [
    {
      "inputs": [],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "uint256",
          "name": "cid",
          "type": "uint256"
        },
        {
          "indexed": true,
          "internalType": "string",
          "name": "code",
          "type": "string"
        },
        {
          "indexed": false,
          "internalType": "enum MarkList.Grades",
          "name": "grades",
          "type": "uint8"
        }
      ],
      "name": "addMarksEvent",
      "type": "event"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "name": "marks",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "code",
          "type": "string"
        },
        {
          "internalType": "enum MarkList.Grades",
          "name": "grades",
          "type": "uint8"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [],
      "name": "marksCount",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "_code",
          "type": "string"
        },
        {
          "internalType": "enum MarkList.Grades",
          "name": "_grades",
          "type": "uint8"
        }
      ],
      "name": "addMarks",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "_code",
          "type": "string"
        }
      ],
      "name": "findMarks",
      "outputs": [
        {
          "components": [
            {
              "internalType": "uint256",
              "name": "cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "code",
              "type": "string"
            },
            {
              "internalType": "enum MarkList.Grades",
              "name": "grades",
              "type": "uint8"
            }
          ],
          "internalType": "struct MarkList.Marks",
          "name": "",
          "type": "tuple"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    }
  ]