import React, { Component } from "react";
import Web3 from 'web3';
// import './App.css';
import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from "./abi/config_studentList";
import StudentList from "./components/StudentList";
import { SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS } from "./abi/config_subjectList";
import SubjectList from "./components/SubjectList";
import {MARK_LIST_ABI, MARK_LIST_ADDRESS} from "./abi/config_markList"
import MarkList from "./components/MarkList";
import FindMark from "./components/FindMark";

class App extends Component {

  componentDidMount() {
    if (!window.ethereum) 
      throw new Error("No crypto wallet found. please install it");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }

  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:7545")
    const accounts = await web3.eth.getAccounts()
    this.setState({account: accounts[0]})
    // Step 4: load all the lists from the blockchain
    const studentList = new web3.eth.Contract(
      STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS)
    const subjectList = new web3.eth.Contract(
      SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS)
    const markList = new web3.eth.Contract(
      MARK_LIST_ABI, MARK_LIST_ADDRESS)
    // Keep the lists in the current state
    this.setState({studentList})
    this.setState({subjectList})
    this.setState({markList})
    // Get the number of records for all list in the blockchain 
    const studentCount = await studentList.methods.studentsCount().call()
    const subjectCount = await subjectList.methods.subjectsCount().call()
    const markCount = await markList.methods.marksCount().call()
    //Store this value in the current state as well
    this.setState({studentCount})
    this.setState({subjectCount})
    this.setState({markCount})
    // Use an iteration to extract each records info and store them in an array
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.students = []
    for(var i = 1; i <= studentCount; i++) {
      const student = await studentList.methods.students(i).call()
      this.setState({
        students: [...this.state.students, student]
      })
    }
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.subjects = []
    for(var j = 1; j <= subjectCount; j++) {
      const subject = await subjectList.methods.subjects(j).call()
      this.setState({
        subjects: [...this.state.subjects, subject]
      })
    }
  }
  
  constructor(props) {
    super(props) 
    this.state = {
      account: '',
      markCount: 0,
      studentCount: 0,
      subjectCount: 0,
       marks: [],
      students: [],
      subjects: [],
      loading: true,
       update: false,
       updatecid:0,
       studentObj:{}
    };
    this.createStudent = this.createStudent.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
    this.createSubject = this.createSubject.bind(this)
    this.markRetired = this.markRetired.bind(this)
    this.addMarks = this.createMarks.bind(this)
    this.findMarks = this.findMarks.bind(this)
    this.updateName = this.updateName.bind(this);
  }
  createMarks(studentid, subjectid, grade) {
    this.setState({ loading: true})
    this.state.markList.methods.addMarks(studentid, subjectid, grade)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading: false})
      this.loadBlockchainData()
    })
  }
  createStudent(cid, name) {
    this.setState({ loading: true})
    this.state.studentList.methods.createStudent(cid, name)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading: true})
      this.loadBlockchainData()
    })
  }

  updateName(id,name){
    // alert(id+name)
    this.setState({update:true})
    this.state.studentList.methods.updateStudent(id,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false});
      this.setState({update:false});
      this.loadBlockchainData();
    })
    this.setState({loading:false})
    this.setState({update:false})
    // alert(student._id)
  }

  createSubject(code, name) {
    this.setState({ loading: true})
    this.state.subjectList.methods.createSubject(code, name)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading: true})
      this.loadBlockchainData()
    })
  }

  markGraduated(id) {
    this.setState({loading: true})
    this.state.studentList.methods.markedGraduated(id)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading: false})
      this.loadBlockchainData()
    })
  }

  markRetired(code) {
    this.setState({loading: true})
    this.state.subjectList.methods.markRetired(code)
    .send({from: this.state.account})
    .once('receipt', (receipt) => {
      this.setState({loading: false})
      this.loadBlockchainData()
    })
  }

  findMarks(studentid,subjectid) {
    this.setState({loading:true})
    return this.state.markList.methods
    .findMarks(studentid,subjectid)
    .call({from:this.state.account})
  }
  render() {
    return (
      <div className="container">
        <h1>Mark concatenation system</h1>
        <p>Your account: {this.state.account}</p>
        <StudentList createStudent={this.createStudent} updateName={this.updateName} update ={this.state.update} studentObj = {this.studentObj}/>
        <p>Total Student Count: {this.state.studentCount}</p>
        {/* <p>Student: {this.state.students}</p> */}
        <ul id="studentList" className="list-unstyled">
          {
            //This gets the each student from the studenList and pass them into a function that display the details of the student
            this.state.students.map((student, key) => {
              return(
                <li className="list-group-item checkbox" key={key}>
                  <span className="name alert">{student._id} {student.cid} {student.name}</span>
                  <input className="form-check-input" type="checkbox" name={student._id} defaultChecked={student.graduated} disabled={student.graduated} ref={(input) => {
                    this.checkbox = input
                  }}
                  onClick={(event) => {
                    this.markGraduated(event.currentTarget.name)
                  }}/>
                  <label className="form-check-label">Graduated</label>
                  <button className="button btn-primary">update</button>
                </li>
              )
            })
          }
        </ul>
        <h1>Subject Section</h1>
        <SubjectList createSubject={this.createSubject}/>
        <p>Total Subject Count: {this.state.subjectCount}</p>
        <ul id="subjectList" className="list-unstyled">
          {
            //This gets the each student from the studenList and pass them into a function that display the details of the student
            this.state.subjects.map((subject, key) => {
              return(
                <li className="list-group-item checkbox" key={key}>
                  <span className="name alert">{subject._id} {subject.code} {subject.name}</span>
                  <input className="form-check-input" type="checkbox" name={subject._id} defaultChecked={subject.retired} disabled={subject.retired} ref={(input) => {
                    this.checkbox = input
                  }}
                  onClick={(event) => {
                    this.markRetired(event.currentTarget.name)
                  }}/>
                  <label className="form-check-label">Retired</label>
                  {/* <button className="button btn-primary">update</button> */}
                </li>
              )
            })
          }
        </ul>
        <h1>Mark Section</h1>
        <MarkList 
          subjects={this.state.subjects}
          students={this.state.students}
          addMarks={this.addMarks}
        />
        <FindMark
        subjects = {this.state.subjects}
        students = {this.state.students}
        findMarks={this.findMarks}
        
        />
        <p>Total Mark Record: {this.state.markCount}</p>
      </div>
    );
  }
}
export default App;