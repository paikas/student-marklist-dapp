var StudentList = artifacts.require("./StudentList.sol");
var SubjectList = artifacts.require("./SubjectList.sol");
var MarkList = artifacts.require("./MarksList.sol");
module.exports = function(deployer) {
	deployer.deploy(StudentList);
	deployer.deploy(SubjectList);
	deployer.deploy(MarkList);
};
