// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract SubjectList{

    uint public subjectsCount = 0;

    struct Subject{
        uint _id;
        string code;
        string name;
        bool retired;
    }

    //store subject
    mapping(uint => Subject) public subjects;

    //events to create subject
    event createSubjectEvent(
        uint _id,
        string code,
        string name,
        bool retired
    );

    //Create and add subject to storage
    function createSubject(string memory code, string memory _name)
        public returns (Subject memory){
            subjectsCount++;
            subjects[subjectsCount] = Subject(subjectsCount, code, _name, false);

            //trigger
            emit createSubjectEvent(subjectsCount, code, _name, false);
            return subjects[subjectsCount];
        }

    //event for retired status
    event markRetiredEvent(
        uint indexed _id
    );
    
    //change retired status 
    function markRetired(uint _id)public returns(Subject memory){ 
        subjects[_id].retired = true;
        //trigger create event
        emit markRetiredEvent(_id);
        return subjects[_id];
    }

     //fetch subject info from storage
    function findSubject(uint _id) public view returns(Subject memory){
        return subjects[_id];
    }

    function updateSubject(uint _id, string memory _newcode, string memory _newname) public returns (Subject memory) {
        subjects[_id].code = _newcode;
        subjects[_id].name = _newname;
        emit updateSubjectEvent(_id, _newcode, _newname);
        return subjects[_id];
    }
     event updateSubjectEvent(
        uint _id,
        string  _newcode,
        string _newname
        );

}