// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract StudentList{
    
    uint public studentsCount = 0;

    struct Student{
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }

    //store student
    mapping(uint => Student) public students;

    //constructor for students
    // constructor(){
    //     createStudent(1001, "Dorji Dorji");
    // }

    //events
    event createStudentEvent(
        uint _id,
        uint indexed cid,
        string name,
        bool graduated
    );

    //Create and add students to storage
    function createStudent(uint _studentCid, string memory _name)
        public returns (Student memory){
            studentsCount++;
            students[studentsCount] = Student(studentsCount, _studentCid, _name, false);

            //trigger
            emit createStudentEvent(studentsCount, _studentCid, _name, false);
            return students[studentsCount];
        }
    
    //event for graduation status
    event markGraduatedEvent(
        uint indexed cid
    );

    //change graduation status of student
    function markGraduated(uint _id)public returns(Student memory){ 
        students[_id].graduated = true;
        //trigger create event
        emit markGraduatedEvent(_id);
        return students[_id];
    }

    //fetch student info from storage
    function findStudent(uint _id) public view returns(Student memory){
        return students[_id];
    }
    
    
    // //Event for update function
    // event updateStudentEvent(
    //     uint _id,
    //     uint indexed cid,
    //     string _name,
    //     bool graduated
    // );
    // // Update student
    // function updateStudent(uint _id, uint _Cid, string memory _name) public returns(Student memory) {
    //     students[_id] = Student({
    //         _id:_id,
    //         cid : _Cid,
    //         name : _name,
    //         graduated:false
    //     });
    //     emit updateStudentEvent(_id, _Cid, _name, false);
    //     return students[_id];
    // }
    event updateStudentEvent(
    uint _id,
    string _newName
    );

    function updateStudent(uint _id, string memory _newName) public returns (Student memory) {
        // require(_id <= studentsCount, "Invalid student ID"); // make sure the student exists

        students[_id].name = _newName;

        // trigger event to notify that the student's name has been updated
        emit updateStudentEvent(_id, _newName);
        return students[_id];
}

}